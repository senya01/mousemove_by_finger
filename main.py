import cv2
import pyautogui as pag
import mediapipe as mp

# win32api
# 2 потока
# полином, апроксимация
# задержка в i

cap = cv2.VideoCapture(0)
width, height = pag.size()

hands = mp.solutions.hands.Hands(static_image_mode=False, max_num_hands=1,
                                 min_tracking_confidence=0.5, min_detection_confidence=0.5)

violet = (204, 102, 153)

while True:
    res, frame = cap.read()
    # коммент
    h, w = frame.shape[:2]
    result = hands.process(frame)
    if result.multi_hand_landmarks:
        for id, lm in enumerate(result.multi_hand_landmarks[0].landmark):
            cx, cy = int(lm.x * w), int(lm.y * h)
            cv2.circle(frame, (cx, cy), 3, violet, cv2.FILLED)

            if id == 8:
                pag.moveTo(width - cx * width / w, cy * height / h)
                cv2.circle(frame, (cx, cy), 7, violet, cv2.FILLED)

    frame = cv2.flip(frame, 1)
    cv2.imshow("Video", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
